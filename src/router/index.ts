import { createRouter, createWebHashHistory } from 'vue-router'
import { constantRouter } from './routes'
export default createRouter({
  history: createWebHashHistory(),
  routes: constantRouter,
  scrollBehavior() {
    return {
      left: 0,
      top: 0,
    }
  },
})
