export const constantRouter = [
  {
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    name: 'Login',
    meta: {
      title: '登录',
      hidden: true,
    },
  },
  {
    path: '/',
    component: () => import('@/layout/index.vue'),
    name: 'Layout',
    meta: {
      hidden: false,
    },
    redirect: '/home',
    children: [
      {
        path: '/home',
        component: () => import('@/views/home/index.vue'),
        meta: {
          title: '首页',
          hidden: false,
          icon: 'House',
        },
      },
    ],
  },
  {
    path: '/404',
    component: () => import('@/views/404/index.vue'),
    name: '404',
    meta: {
      title: '404',
      hidden: true,
    },
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
    name: 'Any',
    meta: {
      title: '任意',
      hidden: true,
    },
  },
  {
    path: '/explorer',
    component: () => import('@/views/explorer/index.vue'),
    name: 'Screen',
    meta: {
      hidden: false,
      title: '区块链浏览器',
      icon: 'Monitor',
    },
  },
  {
    path: '/sys',
    component: () => import('@/layout/index.vue'),
    name: 'Sys',
    meta: {
      title: '系统管理',
      icon: 'Promotion',
    },
    redirect: '/sys/employee',
    children: [
      {
        path: '/sys/employee',
        component: () => import('@/views/sys/employee/index.vue'),
        name: 'User',
        meta: {
          title: '员工管理',
          icon: 'User',
        },
      },
      {
        path: '/sys/company',
        component: () => import('@/views/sys/company/index.vue'),
        name: 'Company',
        meta: {
          title: '企业管理',
          icon: 'OfficeBuilding',
        },
      },
      {
        path: '/sys/department',
        component: () => import('@/views/sys/department/index.vue'),
        name: 'Department',
        meta: {
          title: '部门管理',
          icon: 'Printer',
        },
      },
    ],
  },
  {
    path: '/contract',
    component: () => import('@/layout/index.vue'),
    name: 'Product',
    meta: {
      title: '电子合同管理',
      icon: 'Document',
    },
    redirect: '/contract/type',
    children: [
      {
        path: '/contract/type',
        component: () => import('@/views/contract/type/index.vue'),
        name: 'Type',
        meta: {
          title: '类型管理',
          icon: 'Orange',
        },
      },
      {
        path: '/contract/template',
        component: () => import('@/views/contract/template/index.vue'),
        name: 'Spu',
        meta: {
          title: '模板管理',
          icon: 'DocumentCopy',
        },
      },
      {
        path: '/contract/manage',
        component: () => import('@/views/contract/manage/index.vue'),
        name: 'Manage',
        meta: {
          title: '合同管理',
          icon: 'Memo',
        },
      },
      {
        path: '/contract/approve',
        component: () => import('@/views/contract/approve/index.vue'),
        name: 'Approve',
        meta: {
          title: '合同审核',
          icon: 'Stamp',
        },
      },
    ],
  },
]
