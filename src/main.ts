import { createApp } from 'vue'
//路由组件
import router from './router'
//引入element plus
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
//@ts-ignore
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'

import App from '@/App.vue'
//全局组件
import gloablComponent from './components/index'
// 引入pinia仓库
import pinia from './store'

const app = createApp(App)
app.use(ElementPlus, {
  locale: zhCn,
})
//svg
import 'virtual:svg-icons-register'
// 引入默认样式
import '@/styles/index.scss'
//路由鉴权
import './permission'
// 注册全局组件
app.use(gloablComponent)
// 注册插件
app.use(router)
app.use(pinia)
app.mount('#app')
