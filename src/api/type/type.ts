export interface ResponseData {
    code: number,
    message: string,
}

export interface TypeData {
    id?: string,
    name: string,
    description: string,
    updateTime: string,
    createTime: string
}

export interface AllTypeListResponseData extends ResponseData{
    data:TypeData[]
  }