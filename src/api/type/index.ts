import request from '@/utils/request.ts'
import type { AllTypeListResponseData, TypeData } from './type.ts'

//项目用户相关的请求地址
enum API {

    ALLTYPE_URL = '/type',

    ADDTYPE_URL = '/type/add',

    UPDATETYPE_URL = '/type/update',

    DELETETYPES_URL = '/type'
}

//所有类型信息
export const reqAllType = (name: string) =>
    request.get<any, AllTypeListResponseData>(API.ALLTYPE_URL + "?name=" + name)

//增添或修改类型信息
export const reqAddAndUpdateType = (data: TypeData) => {
    if (data.id) {
        return request.put<any, any>(API.UPDATETYPE_URL, data)
    }
    else {
        return request.post<any, any>(API.ADDTYPE_URL, data)
    }
}

//批量删除类型信息
export const reqDeleteTypes = (ids: any) =>
    request.post<any, any>(API.DELETETYPES_URL, { data: ids })