import request from '@/utils/request.ts'
import type { loginFormData,loginResponseData,employeeInfoResponseData,AllEmployeeListResponseData,Employee } from './type'

//项目用户相关的请求地址
enum API {
  LOGIN_URL = '/employee/login',

  EMPLOYEEINFO_URL = '/employee/info',

  LOGOUT_URL='/employee/logout',

  ALLEMPLOYEE_URL='/employee/list',

  REMOVEEMPLOYEE_URL='/employee/',

  UPDATEEMPLOYEE_URL='/employee',

  CREATEEMPLOYE_URL='/employee/create',

  CHANGESTATUS_URL='/employee/'
}

//员工登录
export const reqLogin = (data:loginFormData) => {
  return request.post<any, loginResponseData>(API.LOGIN_URL, data)  
}

//员工信息
export const reqEmployeeInfo = () => {
  return request.get<any,employeeInfoResponseData>(API.EMPLOYEEINFO_URL)
}

//员工退出
export const reqLogout =()=>{
  return request.post<any,any>(API.LOGOUT_URL)
}

//所有员工信息
export const reqAllEmployee=(username:string)=>{
  return request.get<any,AllEmployeeListResponseData>(API.ALLEMPLOYEE_URL+"?username="+username)
}

//删除员工信息
export const reqRemoveEmployee=(id:string)=>{
  return request.delete<any,any>(API.REMOVEEMPLOYEE_URL+id)
}

//增添或修改员工信息
export const reqAddAndUpdateEmployee=(data:Employee)=>{
  if(data.id){
    return request.put<any,any>(API.UPDATEEMPLOYEE_URL,data)
  }
  else{
    return request.post<any,any>(API.CREATEEMPLOYE_URL,data)
  }
}

//启/禁员工
export const reqChangeStatus=(id:string)=>{
  return request.put<any,any>(API.CHANGESTATUS_URL+id)
}