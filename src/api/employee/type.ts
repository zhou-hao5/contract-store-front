export interface loginFormData {
  username: string
  password: string
}

export interface ResponseData {
  code:number,
  message:string,
}

export interface loginResponseData extends ResponseData{
  data:string
}

export interface employeeInfoResponseData extends ResponseData{
  data: string,
}

export interface Employee{
  id?:string,
  name?: string,
  username: string,
  password?:string
  phone: string,
  sex: string,
  status?: number,
  createTime?: string,
  updateTime?: string,
}

export interface AllEmployeeListResponseData extends ResponseData{
  data:Employee[]
}


