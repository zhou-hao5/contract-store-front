export interface ResponseData {
    code: number,
    message: string,
}

export interface PageParam {
    name: string,
    companyName: string,
    departmentId: string,
    state:string,
    page: number,
    pageSize: number,
}

export interface ContractData {
    id?: number,
    name: string,
    typeName: string,
    kind: string,
    companyName: string,
    departmentName: string,
    secret: number,
    description: string,
    blockchainId: string,
    ossPath: string,
    isDelete: number,
    hashCode: string,
    state: string,
    info?: string|null,
    auditor?: string|null,
    ipfsPath?: string|null,
    inChain: number,
    txid?:string|null,
    blockNumber?:string|null
    approveTime?:string|null,
    updateTime: string,
    createTime: string
}

export interface AddContractData{
    companyName: string,
	departmentId: string,
	description: string,
	kind: number,
	name: string,
	ossPath: string,
	secret: number,
	typeId: string
}

//数组:元素都是已有合同数据类型
export type Records = ContractData[]

//定义获取已有的模板接口返回的数据ts类型
export interface AllContractResponseData extends ResponseData {
    data: {
        records: Records
        total: number
        size: number
        current: number
        searchCount: boolean
        pages: number
    }
}