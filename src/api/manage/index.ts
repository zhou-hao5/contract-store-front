import request from '@/utils/request.ts'
import { PageParam, AllContractResponseData, AddContractData } from './type'

//项目合同相关的请求地址
enum API {

    ALLCONTRACT_URL = '/contract/page',

    ADDCONTRACT_URL = 'contract/add',

    DELETECONTRACT_URL = 'contract/'
}

//获取合同分页信息
export const reqAllContract = (data: PageParam) =>
    request.post<any, AllContractResponseData>(API.ALLCONTRACT_URL, data)

//获取合同分页信息
export const reqAddContract = (data: AddContractData) =>
    request.post<any, any>(API.ADDCONTRACT_URL, data)

//删除合同信息
export const reqDeleteContract = (id:string) =>
    request.delete<any, any>(API.DELETECONTRACT_URL+id)