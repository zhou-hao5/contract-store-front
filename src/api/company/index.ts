import { reqRemoveEmployee } from './../employee/index';
import request from '@/utils/request.ts'
import { AllCompanyResponseData, CompanyData, PageAndParam } from './type'

//项目企业相关的请求地址
enum API {
    ALLCOMPANY_URL = '/company/page',

    ADDCOMPANY_URL = '/company/add',

    UPDATECOMPANY_URL = '/company/update',

    REMOVECOMPANY_URL='/company/'

}

//获取企业分页信息
export const reqAllCompany = (data: PageAndParam) =>
    request.post<any, AllCompanyResponseData>(API.ALLCOMPANY_URL, data)

//添加或修改企业信息
export const reqAddAndUpdateCompany = (data: CompanyData) => {
    if (data.id) {
        return request.put<any, any>(API.UPDATECOMPANY_URL, data)
    }
    else {
        return request.post<any, any>(API.ADDCOMPANY_URL, data)
    }
}

//删除企业信息
export const reqRemoveCompany = (id:string) =>
    request.delete<any, AllCompanyResponseData>(API.REMOVECOMPANY_URL+id)
