//服务器全部接口返回的数据类型
export interface ResponseData {
    code: number
    message: string
}

export interface PageAndParam {
    name: string,
    page: number,
    pageSize: number,
    registerNumber: string,
    type: string

}

//企业的ts数据类型
export interface CompanyData {
    id?: string,
    legal: string,
    name: string,
    address: string,
    phone: string,
    licence: string,
    registerNumber: string,
    scope: string,
    type: string,
    updateTime?: string
    beginTime: string,
    createTime?: string,
    description: string,
}

//数组:元素都是已有企业数据类型
export type Records = CompanyData[]

//定义获取已有的企业接口返回的数据ts类型
export interface AllCompanyResponseData extends ResponseData {
    data: {
        records: Records
        total: number
        size: number
        current: number
        searchCount: boolean
        pages: number
    }
}