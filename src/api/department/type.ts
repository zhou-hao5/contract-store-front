export interface ResponseData {
    code: number,
    message: string,
}

export interface DepartmentData {
    id?: string,
    name: string,
    amount: string,
    description: string,
    updateTime: string,
    createTime: string
}

export interface AllDepartmentListResponseData extends ResponseData{
    data:DepartmentData[]
}