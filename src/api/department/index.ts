import request from '@/utils/request.ts'
import { AllDepartmentListResponseData, DepartmentData } from './type'

//项目用户相关的请求地址
enum API {

    ALLDEPARTMENT_URL = '/department',

    ADDDEPARTMENT_URL = '/department/add',

    UPDATEDEPARTMENT_URL = '/department/update',

    DELETEDEPARTMENTS_URL = '/department'
}

//所有部门信息
export const reqAllDepartment = (name: string) =>
    request.get<any, AllDepartmentListResponseData>(API.ALLDEPARTMENT_URL + "?name=" + name)

//增添或修改部门信息
export const reqAddAndUpdateDepartment = (data: DepartmentData) => {
    if (data.id) {
        return request.put<any, any>(API.UPDATEDEPARTMENT_URL, data)
    }
    else {
        return request.post<any, any>(API.ADDDEPARTMENT_URL, data)
    }
}

//批量删除部门信息
export const reqDeleteDepartments = (ids: any) =>
    request.post<any, any>(API.DELETEDEPARTMENTS_URL, { data: ids })