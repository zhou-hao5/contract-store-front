import request from '@/utils/request.ts'
import {ApproveParam} from './type'

//项目合同相关的请求地址
enum API {

    APPROVECONTRACT_URL = '/contract/approve',

}


//获取合同分页信息
export const reqApproveContract = (data:ApproveParam ) =>
    request.post<any, any>(API.APPROVECONTRACT_URL, data)