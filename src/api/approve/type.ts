export interface ResponseData {
    code: number,
    message: string,
}

export interface ApproveParam {
    id:string,
    state: string,
    info: string,
}