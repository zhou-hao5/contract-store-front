export interface ResponseData {
    code: number,
    message: string,
}

export interface PageParam {
    name: string,
    kind: number,
    typeId: string,
    page: number,
    pageSize: number,
}

export interface TempalteData {
    id?: string,
    name: string,
    kind: number,
	path: string,
	typeId: string,
    typeName?:string,
    description: string,
    updateTime?: string,
    createTime?: string
}


//数组:元素都是已有模板数据类型
export type Records = TempalteData[]

//定义获取已有的模板接口返回的数据ts类型
export interface AllTemplateResponseData extends ResponseData {
    data: {
        records: Records
        total: number
        size: number
        current: number
        searchCount: boolean
        pages: number
    }
}