import request from '@/utils/request.ts'
import { AllTemplateResponseData, TempalteData, PageParam } from './type'

//项目用户相关的请求地址
enum API {

    ALLTEMPLATE_URL = '/formwork/page',

    ADDTEMPLATE_URL = '/formwork/add',

    UPDATETEMPLATE_URL = '/formwork/update',

    DELETETEMPLATE_URL = '/formwork/'
}

//增添或修改部门信息
export const reqAddAndUpdateDepartment = (data: TempalteData) => {
    if (data.id) {
        return request.put<any, any>(API.UPDATETEMPLATE_URL, data)
    }
    else {
        return request.post<any, any>(API.ADDTEMPLATE_URL, data)
    }
}

//获取模板分页信息
export const reqAllTemplate = (data: PageParam) =>
    request.post<any, AllTemplateResponseData>(API.ALLTEMPLATE_URL, data)

//删除模板信息
export const reqDeleteTemplate = (id: string) =>
    request.delete<any, any>(API.DELETETEMPLATE_URL + id)