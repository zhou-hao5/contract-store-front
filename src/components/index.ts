import { App, Component } from 'vue'
import SvgIcon from './SvgIcon/index.vue'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
const components: { [name: string]: Component } = { SvgIcon }
export default {
  install(app: App) {
    //注册项目全部的全局组件
    Object.keys(components).forEach((key: string) => {
      app.component(key, components[key])
    })
    //注册element-plus提供的图标组件
    for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
      app.component(key, component)
    }
  },
}
