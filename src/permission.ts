//路由鉴权
import router from '@/router'
import { RouteLocationNormalized } from 'vue-router'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'
import useUserStore from '@/store/modules/employee'
import pinia from './store'
import setting from './setting'

nprogress.configure({ showSpinner: false })
let userStore = useUserStore(pinia)
router.beforeEach(
    async (
        to: RouteLocationNormalized,
        from: RouteLocationNormalized,
        next: any,
    ) => {
        document.title = setting.title + '-' + to.meta.title
        nprogress.start()
        let token = userStore.token
        console.log(from);    
        if (token) {
            if (to.path == '/login') {
                next({ path: '/' })
            } else {
                if (userStore.username) {
                    next()
                } else {
                    try {
                        await userStore.userInfo()
                        next()
                    } catch (error) {
                        userStore.userLogout()
                        next({ path: '/login' })
                    }
                }
            }
        } else {
            if (to.path == '/login') {
                next()
            } else {
                next({ path: '/login' })
            }
        }
    },
)

router.afterEach(
    (to: RouteLocationNormalized, from: RouteLocationNormalized, next: any) => {
       console.log(to,from,next);    
        nprogress.done()
    },
)
