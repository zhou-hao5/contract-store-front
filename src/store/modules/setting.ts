//layout组件相关仓库
import { defineStore } from 'pinia'
import { ref } from 'vue'

export default defineStore('layoutSetting', () => {
  let fold = ref(false)
  let refresh = ref(false)
  return { fold, refresh }
})
