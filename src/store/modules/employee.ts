//用户相关仓库
import { defineStore } from 'pinia'
import { reqLogin, reqEmployeeInfo,reqLogout } from '@/api/employee'
import { ref } from 'vue'
import { constantRouter } from '@/router/routes'
import type { loginFormData,loginResponseData,userInfoResponseData } from '@/api/employee/type'

//项目用户相关的请求地址
export default defineStore('Employee', () => {
  let menuRoutes = ref(constantRouter)
  let token = ref(localStorage.getItem('TOKEN'))
  let username = ref('')
  //用户登录
  const userLogin = async (data:loginFormData) => {
    //成功 0->token
    //失败->登录错误信息
    let res: loginResponseData = await reqLogin(data)
    if (res.code == 0) {
      token.value = res.data
      localStorage.setItem('TOKEN', res.data)
      return 'ok'
    } else {
      throw new Error(res.message)
    }
  }
  //获取用户信息
  const userInfo = async () => {
    let res:userInfoResponseData = await reqEmployeeInfo()
    if (res.code == 0) {
      username.value = res.data
    } else {
      throw new Error(res.message)
    }
  }

  //退出登录
  const userLogout = async() => {
    let res=await reqLogout()
    if(res.code==0){
      token.value = ''
      username.value = ''
      localStorage.removeItem('TOKEN')
      return 'ok'
    }else{
      throw new Error(res.message)
    }
    
  }

  return {
    userLogin,
    userInfo,
    menuRoutes,
    token,
    username,
    userLogout,
  }
})
